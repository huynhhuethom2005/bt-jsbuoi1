/*  
BT1: 
- input: Số ngày
- output: Số tiền lương
- processing: 
+ Gán biến luongNgay = 100
+ Tạo biến soNgay để người dùng nhập
+ Tạo biến tongLuong = luongNgay * soNgay
*/

const LUONGNGAY = 100000
var soNgay = 50
var tongLuong = LUONGNGAY * soNgay
console.log("🚀 ~ file: index.js ~ line 14 ~ tongLuong", tongLuong)

/*
BT2: 
- input: 5 số nguyên
- output: giá trị trung bình của 5 số
- processing:
+ Tạo 5 biến 
+ Tạo biến trungBinh = (so1+so2+so3+so4+so5)/5
*/

var so1 = 1, so2 = 2, so3 = 3, so4 = 4, so5 = 5
var trungBinh = (so1 + so2 + so3 + so4 + so5) / 5
console.log("🚀 ~ file: index.js ~ line 27 ~ trungBinh", trungBinh)

/*
BT3: 
- input: Số tiền USD
- output: Số tiền VND
- processing: 
+ tạo biến giaUSD = 23500, nhapUSD
+ Nhân USD và nhapUSD
*/

const GIAUSD = 23500
var nhapUSD = 22
var quydoiVND = GIAUSD * nhapUSD
console.log("🚀 ~ file: index.js ~ line 41 ~ quydoiVND", quydoiVND, "VND")

/* 
BT4:
- input: chiều dài, chiều rộng
- output: chu vi, diện tích
- processing: tính theo công thức chi vi và diện tích
*/

var chieuDai = 3, chieuRong = 4
var chuVi = (chieuDai + chieuRong) * 2
console.log("🚀 ~ file: index.js ~ line 52 ~ chuVi", chuVi)

var dienTich = chieuDai * chieuRong
console.log("🚀 ~ file: index.js ~ line 54 ~ dienTich", dienTich)

/*
BT5: 
- input: số có 2 chữ số
- output: tổng của 2 ký số
- processing:
+ Chia lấy phần dư với 10 để lấy hàng đơn vị
+ Dùng math.floor để lấy hàng chục
*/

var soNguyen = 23
var donVi = 23 % 10
var hangChuc = Math.floor(23 / 10)
var Tong = donVi + hangChuc
console.log("🚀 ~ file: index.js ~ line 70 ~ Tong", Tong)
